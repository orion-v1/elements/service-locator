﻿using NUnit.Framework.Constraints;
using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public class ServiceConstraint : Constraint
    {
        private readonly Type _expectedType;
        private readonly int _expectedServiceId;
        private readonly int _expectedObjectId;

        public ServiceConstraint(Type expectedType, int expectedServiceId, int expectedObjectId = -1)
        {
            _expectedType = expectedType;
            _expectedServiceId = expectedServiceId;
            _expectedObjectId = expectedObjectId;

            Description = expectedObjectId < 0 ?
                ServiceTestUtils.GetServiceDescription(expectedType, expectedServiceId) :
                ServiceTestUtils.GetServiceDescription(expectedType, expectedServiceId, expectedObjectId);
        }

        public override ConstraintResult ApplyTo(object actual)
        {
            var service = actual as IService;
            if (service == null)
                return new ConstraintResult(this, actual, false);

            if (service.GetType() != _expectedType)
                return new ConstraintResult(this, actual, false);

            return new ConstraintResult(this, actual, service.ServiceId == _expectedServiceId);
        }
    }
}
