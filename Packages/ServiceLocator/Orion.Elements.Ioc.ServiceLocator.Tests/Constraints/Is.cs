﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public class Is : NUnit.Framework.Is
    {
        public static ServiceConstraint Service(Type expectedType, int expectedServiceId, int expectedObjectId = -1)
        {
            return new ServiceConstraint(expectedType, expectedServiceId, expectedObjectId);
        }

        public static ServiceConstraint Service<T>(int expectedServiceId, int expectedObjectId = -1)
        {
            return new ServiceConstraint(typeof(T), expectedServiceId, expectedObjectId);
        }
    }
}
