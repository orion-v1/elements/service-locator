﻿using NUnit.Framework.Constraints;
using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public static class ConstraintExpressionExtensions
    {
        public static ServiceConstraint Service(this ConstraintExpression expression, Type expectedType, int expectedServiceId)
        {
            var constraint = new ServiceConstraint(expectedType, expectedServiceId);
            expression.Append(constraint);
            return constraint;
        }

        public static ServiceConstraint Service<T>(this ConstraintExpression expression, int expectedServiceId)
        {
            var constraint = new ServiceConstraint(typeof(T), expectedServiceId);
            expression.Append(constraint);
            return constraint;
        }
    }
}
