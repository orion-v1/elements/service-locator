﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    public interface IServiceB : IService
    {
        void Log();
    }
}
