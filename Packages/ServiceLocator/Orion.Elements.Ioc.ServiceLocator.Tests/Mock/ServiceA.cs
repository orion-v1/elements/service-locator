﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    public class ServiceA : ServiceBase, IServiceA
    {
    }

    public class ServiceA1 : ServiceBase, IServiceA
    {
    }

    public class ServiceA2 : ServiceBase, IServiceA
    {
    }
}
