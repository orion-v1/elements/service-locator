﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public class PotentiallyAmbiguousConstructorService : ServiceBase
    {
        public IServiceContainer Container { get; }
        public IServiceProvider Provider { get; }
        public IServiceScopeFactory ScopeFactory { get; }
        public int IntValue { get; }


        public PotentiallyAmbiguousConstructorService(IServiceContainer container, IServiceProvider provider, int intValue)
        {
            Container = container;
            Provider = provider;
            IntValue = intValue;
        }

        public PotentiallyAmbiguousConstructorService(IServiceContainer container, IServiceProvider provider, IServiceScopeFactory scopeFactory)
        {
            Container = container;
            Provider = provider;
            ScopeFactory = scopeFactory;
        }

        public override string ToString()
        {
            return $"{base.ToString()}\nContainer: {Container}\nProvider: {Provider}\nScope Factory: {ScopeFactory}\nInt Value: {IntValue}";
        }
    }
}
