﻿using NUnit.Framework;
using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public abstract class ServiceBase : IService, IDisposable
    {
        public int ObjectId { get; }
        public int ServiceId { get; }
        public bool IsDisposed { get; private set; }

        public ServiceBase()
        {
            ObjectId = ServiceTestUtils.RequestObjectId();
            ServiceId = ServiceTestUtils.RequestServiceId(GetType());
        }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public virtual void Log()
        {
            TestContext.Out.WriteLine(ToString());
        }

        public override string ToString()
        {
            string desc = ServiceTestUtils.GetServiceDescription(GetType(), ServiceId, ObjectId);

            return IsDisposed ?
                $"{desc} - Disposed" :
                desc;
        }
    }
}
