﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    public interface IService
    {
        int ServiceId { get; }
        int ObjectId { get; }

        bool IsDisposed { get; }
    }
}
