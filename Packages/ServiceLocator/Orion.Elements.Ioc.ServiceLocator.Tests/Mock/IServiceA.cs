﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    public interface IServiceA : IService
    {
        void Log();
    }
}
