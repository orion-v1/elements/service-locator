﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public class MultipleConstructorService : ServiceBase
    {
        public IServiceContainer Container { get; }
        public IServiceProvider Provider { get; }
        public IServiceScopeFactory ScopeFactory { get; }

        public MultipleConstructorService(IServiceContainer container)
        {
            Container = container;
        }

        public MultipleConstructorService(IServiceContainer container, IServiceProvider provider)
        {
            Container = container;
            Provider = provider;
        }

        public MultipleConstructorService(IServiceContainer container, IServiceProvider provider, IServiceScopeFactory scopeFactory)
        {
            Container = container;
            Provider = provider;
            ScopeFactory = scopeFactory;
        }

        public override string ToString()
        {
            return $"{base.ToString()}\nContainer: {Container}\nProvider: {Provider}\nScope Factory: {ScopeFactory}";
        }
    }
}
