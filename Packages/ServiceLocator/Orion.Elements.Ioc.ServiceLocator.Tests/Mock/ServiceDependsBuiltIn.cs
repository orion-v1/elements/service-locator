﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public class ServiceDependsBuiltIn : ServiceBase
    {
        public IServiceContainer Container { get; }
        public IServiceProvider Provider { get; }
        public IServiceScopeFactory ScopeFactory { get; }

        public ServiceDependsBuiltIn(IServiceContainer container, IServiceProvider provider, IServiceScopeFactory scopeFactory)
        {
            Container = container;
            Provider = provider;
            ScopeFactory = scopeFactory;
        }

        public override string ToString()
        {
            return $"{base.ToString()}\nContainer: {Container}\nProvider: {Provider}\nScope Factory: {ScopeFactory}";
        }
    }
}
