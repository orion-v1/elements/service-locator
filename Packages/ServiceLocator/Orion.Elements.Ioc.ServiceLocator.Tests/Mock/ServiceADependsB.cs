﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    public class ServiceADependsB : ServiceBase, IServiceA
    {
        private readonly IServiceB _service;

        public ServiceADependsB(IServiceB service)
        {
            _service = service;
        }

        public override string ToString()
        {
            return $"{base.ToString()} Service B = {_service?.ToString()}";
        }
    }
}
