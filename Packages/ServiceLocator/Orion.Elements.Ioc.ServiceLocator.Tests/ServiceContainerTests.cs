﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Orion.Elements.Ioc.ServiceLocator
{
    [TestFixture]
    public class ServiceContainerTests
    {
        [SetUp]
        public void Init()
        {
            ServiceTestUtils.Reset();
        }

        #region GetService - Liftime

        [Test]
        public void GetService_Transient_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1, Is.Service<ServiceA>(0));
            Assert.That(service2, Is.Service<ServiceA>(1));
        }

        [Test]
        public void GetService_Scoped_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Scoped).Apply();

            IServiceA service1;
            IServiceA service2;
            using (var scope = c.CreateScope())
            {
                service1 = scope.GetService<IServiceA>();
                service2 = scope.GetService<IServiceA>();
            }

            IServiceA service3;
            IServiceA service4;
            using (var scope = c.CreateScope())
            {
                service3 = scope.GetService<IServiceA>();
                service4 = scope.GetService<IServiceA>();
            }

            ServiceTestUtils.LogServices(service1, service2, service3, service4);
            Assert.That(service1, Is.Service<ServiceA>(0));
            Assert.That(service2, Is.Service<ServiceA>(0));
            Assert.That(service3, Is.Service<ServiceA>(1));
            Assert.That(service4, Is.Service<ServiceA>(1));
        }

        [Test]
        public void GetService_Singleton_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Singleton).Apply();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            IServiceA service3;
            IServiceA service4;
            using (var scope = c.CreateScope())
            {
                service3 = scope.GetService<IServiceA>();
                service4 = scope.GetService<IServiceA>();
            }

            ServiceTestUtils.LogServices(service1, service2, service3, service4);
            Assert.That(service1, Is.Service<ServiceA>(0));
            Assert.That(service2, Is.Service<ServiceA>(0));
            Assert.That(service3, Is.Service<ServiceA>(0));
            Assert.That(service4, Is.Service<ServiceA>(0));
        }

        #endregion

        #region GetService - Collections

        [Test]
        public void GetService_Collection_ReturnsLast()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA1>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceA2>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();

            var service1 = c.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1);
            Assert.That(service1, Is.Service<ServiceA2>(0));
        }

        [Test]
        public void GetService_Collection_ReturnsAll()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA1>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceA2>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();

            IReadOnlyList<IServiceA> services = c.GetService<IReadOnlyList<IServiceA>>();

            ServiceTestUtils.LogServices(services);
            Assert.That(services, Is.Not.Null);
            Assert.That(services.Count, Is.EqualTo(2));
            Assert.That(services[0], Is.Service<ServiceA1>(0, 0));
            Assert.That(services[1], Is.Service<ServiceA2>(0, 1));
        }

        #endregion

        #region GetService - Multiple Binding

        [Test]
        public void GetService_MultipleBinding_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA1>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Scoped).Apply();
            c.CreateService<ServiceA2>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Scoped).Apply();
            c.CreateService<ServiceB1>().BindTo<IServiceB>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceB2>().BindTo<IServiceB>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceAB>()
                .BindTo<IServiceA>()
                .BindTo<IServiceB>()
                .SetLifetime(ServiceLifetime.Scoped)
                .Apply();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceB>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1, Is.Service<ServiceAB>(0));
            Assert.That(service2, Is.Service<ServiceAB>(0));
        }

        [Test]
        public void GetService_MultipleBinding_ReturnsAll()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA1>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Scoped).Apply();
            c.CreateService<ServiceA2>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Scoped).Apply();
            c.CreateService<ServiceB1>().BindTo<IServiceB>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceB2>().BindTo<IServiceB>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceAB>()
                .BindTo<IServiceA>()
                .BindTo<IServiceB>()
                .SetLifetime(ServiceLifetime.Scoped)
                .Apply();

            var services1 = c.GetServices<IServiceA>();
            var services2 = c.GetServices<IServiceB>();

            ServiceTestUtils.LogServices(services1);
            ServiceTestUtils.LogServices(services2);

            Assert.That(services1, Is.Not.Null);
            Assert.That(services1.Count, Is.EqualTo(3));
            Assert.That(services1[0], Is.Service<ServiceA1>(0));
            Assert.That(services1[1], Is.Service<ServiceA2>(0));
            Assert.That(services1[2], Is.Service<ServiceAB>(0));

            Assert.That(services2, Is.Not.Null);
            Assert.That(services2.Count, Is.EqualTo(3));
            Assert.That(services2[0], Is.Service<ServiceB1>(0));
            Assert.That(services2[1], Is.Service<ServiceB2>(0));
            Assert.That(services2[2], Is.Service<ServiceAB>(0));
        }

        #endregion

        #region GetService - Implementation Instance

        [Test]
        public void GetService_AbstractWithImplementationInstance_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>()
                .BindTo<IService>()
                .SetImplementationInstance(new ServiceA())
                .Apply();

            var service1 = c.GetService<IService>();
            var service2 = c.GetService<IService>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1, Is.Service<ServiceA>(0));
            Assert.That(service2, Is.Service<ServiceA>(0));
        }

        [Test]
        public void GetService_GenericWithImplementationInstance_ReturnsOne()
        {
            var list = new List<string> { "first", "second", "third" };
            ServiceContainer c = new ServiceContainer();
            c.CreateService<IList<string>>()
                .BindTo<IList<string>>()
                .SetImplementationInstance(list)
                .Apply();

            var col = c.GetService<IList<string>>();

            Assert.That(col, Is.Not.Null);
            Assert.That(col.Count, Is.EqualTo(list.Count));
            for (int i = 0; i < col.Count; i++)
                Assert.That(col[i], Is.EqualTo(list[i]));
        }

        #endregion

        #region GetService - Implementation Factory

        [Test]
        public void GetService_AbstractWithFactory_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>()
                .BindTo<IService>()
                .SetImplementationFactory(_ => new ServiceA())
                .Apply();

            var service1 = c.GetService<IService>();
            var service2 = c.GetService<IService>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1, Is.Service<ServiceA>(0));
            Assert.That(service2, Is.Service<ServiceA>(0));
        }

        [Test]
        public void GetService_GenericWithFactory_ReturnsOne()
        {
            Func<IServiceProvider, IList<string>> factory = (provider) => new List<string> { "one", "two", "three" };
            var list = factory(null);

            ServiceContainer c = new ServiceContainer();
            c.CreateService<IList<string>>()
                .BindTo<IList<string>>()
                .SetImplementationFactory(factory)
                .Apply();

            var col = c.GetService<IList<string>>();

            Assert.That(col, Is.Not.Null);
            Assert.That(col.Count, Is.EqualTo(list.Count));
            for (int i = 0; i < col.Count; i++)
                Assert.That(col[i], Is.EqualTo(list[i]));
        }

        #endregion

        #region GetService - DI: Constuctor Injection

        [Test]
        public void GetService_DependsBuiltIn_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceDependsBuiltIn>().BindTo<ServiceDependsBuiltIn>().SetLifetime(ServiceLifetime.Transient).Apply();

            var service = c.GetService<ServiceDependsBuiltIn>();

            ServiceTestUtils.LogServices(service);
            Assert.That(service, Is.Service<ServiceDependsBuiltIn>(0));
            Assert.That(service.Container, Is.Not.Null);
            Assert.That(service.Provider, Is.Not.Null);
            Assert.That(service.ScopeFactory, Is.Not.Null);
        }

        [Test]
        public void GetService_DependsSelf_Throws()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceDependsSelf>().BindTo<ServiceDependsSelf>().SetLifetime(ServiceLifetime.Transient).Apply();

            Assert.That(
                () => c.GetService<ServiceDependsSelf>(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetService_CircularDependency_Throws()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceADependsB>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply();
            c.CreateService<ServiceBDependsA>().BindTo<IServiceB>().SetLifetime(ServiceLifetime.Transient).Apply();

            Assert.That(
                () => c.GetService<IServiceA>(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetService_MultipleConstructor_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<MultipleConstructorService>().BindTo<MultipleConstructorService>().SetLifetime(ServiceLifetime.Transient).Apply();

            var service = c.GetService<MultipleConstructorService>();

            ServiceTestUtils.LogServices(service);
            Assert.That(service, Is.Service<MultipleConstructorService>(0));
            Assert.That(service.Container, Is.Not.Null);
            Assert.That(service.Provider, Is.Not.Null);
            Assert.That(service.ScopeFactory, Is.Not.Null);
        }

        [Test]
        public void GetService_AmbiguousConstructor_Throws()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<AmbiguousConstructorService>().BindTo<AmbiguousConstructorService>().SetLifetime(ServiceLifetime.Transient).Apply();

            Assert.That(
                () => c.GetService<AmbiguousConstructorService>(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetService_PotentiallyAmbiguousConstructor_Throws()
        {
            ServiceContainer c = new ServiceContainer(new ServiceContainerOptions { AllowToInjectDefaults = true });
            c.CreateService<PotentiallyAmbiguousConstructorService>().BindTo<PotentiallyAmbiguousConstructorService>().SetLifetime(ServiceLifetime.Transient).Apply();

            Assert.That(
                () => c.GetService<PotentiallyAmbiguousConstructorService>(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetService_PotentiallyAmbiguousConstructor_ReturnsOne()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<PotentiallyAmbiguousConstructorService>().BindTo<PotentiallyAmbiguousConstructorService>().SetLifetime(ServiceLifetime.Transient).Apply();

            var service = c.GetService<PotentiallyAmbiguousConstructorService>();

            ServiceTestUtils.LogServices(service);
            Assert.That(service, Is.Service<PotentiallyAmbiguousConstructorService>(0));
            Assert.That(service.Container, Is.Not.Null);
            Assert.That(service.Provider, Is.Not.Null);
            Assert.That(service.ScopeFactory, Is.Not.Null);
            Assert.That(service.IntValue, Is.EqualTo(0));
        }

        #endregion

        #region AddService

        [Test]
        public void AddService_InvalidImplementationType_Throws()
        {
            ServiceContainer c = new ServiceContainer();

            Assert.That(
                () => c.CreateService<ServiceB>().BindTo<IServiceA>().SetLifetime(ServiceLifetime.Transient).Apply(),
                Throws.Exception.TypeOf<InvalidCastException>());
        }

        [Test]
        public void AddService_InvalidLifetime_Throws()
        {
            ServiceContainer c = new ServiceContainer();

            Assert.That(
                () => c.CreateService()
                    .SetImplementationInstance(new ServiceA())
                    .BindTo<IServiceA>()
                    .SetLifetime(ServiceLifetime.Transient)
                    .Apply(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void AddService_AmbiguousImplementation_Throws()
        {
            ServiceContainer c = new ServiceContainer();

            Assert.That(
                () => c.CreateService()
                    .SetImplementationInstance(new ServiceA())
                    .SetImplementationFactory(provider => new ServiceA())
                    .BindTo<IServiceA>()
                    .Apply(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void AddService_DoubleRegistration_Throws()
        {
            ServiceContainer c = new ServiceContainer();
            c.AddSingletonService<ServiceA, IServiceA>();

            Assert.That(
                () => c.AddSingletonService<ServiceA, IServiceA>(),
                Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void AddService_MultipleBinding_BindsAll()
        {
            ServiceContainer c = new ServiceContainer();
            c.AddSingletonService<ServiceA, IServiceA>();
            c.AddSingletonService<ServiceAB, IServiceA>();

            var services = c.GetService<IReadOnlyList<IServiceA>>();

            ServiceTestUtils.LogServices(services);
            Assert.That(services, Is.Not.Null);
            Assert.That(services.Count, Is.EqualTo(2));
            Assert.That(services[0], Is.Service<ServiceA>(0, 0));
            Assert.That(services[1], Is.Service<ServiceAB>(0, 1));
        }

        [Test]
        public void AddService_MultipleBindingToPermanentService_Throws()
        {
            ServiceContainer c = new ServiceContainer();
            c.AddSingletonService<ServiceA, IServiceA>(ServiceBindingOptions.Exclusive);

            Assert.That(
                () => c.AddSingletonService<ServiceAB, IServiceA>(),
                Throws.Exception.TypeOf<InvalidOperationException>());

            var services = c.GetService<IReadOnlyList<IServiceA>>();

            ServiceTestUtils.LogServices(services);
            Assert.That(services, Is.Not.Null);
            Assert.That(services.Count, Is.EqualTo(1));
            Assert.That(services[0], Is.Service<ServiceA>(0, 0));
        }

        #endregion

        #region RemoveService

        [Test]
        public void RemoveService_ByDescriptor_RemovesOne()
        {
            ServiceContainer c = new ServiceContainer();

            var descriptor = new ServiceDescriptor<ServiceA>(
                ServiceLifetime.Singleton, 
                new[] { new ServiceBinding { Type = typeof(IServiceA), Options = ServiceBindingOptions.Default } });

            c.AddSingletonService<ServiceA1, IServiceA>();
            c.AddSingletonService<ServiceA2, IServiceA>();
            c.AddService(descriptor);

            bool result = c.RemoveService(descriptor);
            var services = c.GetService<IReadOnlyList<IServiceA>>();

            ServiceTestUtils.LogServices(services);
            Assert.That(result, Is.True);
            Assert.That(services, Is.Not.Null);
            Assert.That(services.Count, Is.EqualTo(2));
            Assert.That(services[0], Is.Service<ServiceA1>(0));
            Assert.That(services[1], Is.Service<ServiceA2>(0));
        }

        [Test]
        public void RemoveService_ByDescriptor_RemovesNothing()
        {
            ServiceContainer c = new ServiceContainer();

            var descriptor = new ServiceDescriptor<ServiceA>(
                ServiceLifetime.Singleton,
                new[] { new ServiceBinding { Type = typeof(IServiceA), Options = ServiceBindingOptions.Default } });

            c.AddSingletonService<ServiceA1, IServiceA>();
            c.AddSingletonService<ServiceA2, IServiceA>();

            bool result = c.RemoveService(descriptor);
            var services = c.GetService<IReadOnlyList<IServiceA>>();

            ServiceTestUtils.LogServices(services);
            Assert.That(result, Is.False);
            Assert.That(services, Is.Not.Null);
            Assert.That(services.Count, Is.EqualTo(2));
            Assert.That(services[0], Is.Service<ServiceA1>(0));
            Assert.That(services[1], Is.Service<ServiceA2>(0));
        }

        #endregion

        #region Dispose Container

        [Test]
        public void DisposeContainer_CaptureTransient_Disposed()
        {
            ServiceContainer c = new ServiceContainer(new ServiceContainerOptions { CaptureDisposableTransientServices = true });
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsTransient();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            c.Dispose();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.True);
            Assert.That(service2.IsDisposed, Is.True);
        }

        [Test]
        public void DisposeContainer_IgnoreTransient_NotDisposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsTransient();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            c.Dispose();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.False);
            Assert.That(service2.IsDisposed, Is.False);
        }

        [Test]
        public void DisposeContainer_CaptureScoped_Disposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsScoped();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            c.Dispose();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.True);
            Assert.That(service2.IsDisposed, Is.True);
        }

        [Test]
        public void DisposeContainer_CaptureSingleton_Disposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsSingleton();

            var service1 = c.GetService<IServiceA>();
            var service2 = c.GetService<IServiceA>();

            c.Dispose();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.True);
            Assert.That(service2.IsDisposed, Is.True);
        }

        #endregion

        #region Dispose Scope

        [Test]
        public void DisposeScope_CaptureTransient_Disposed()
        {
            ServiceContainer c = new ServiceContainer(new ServiceContainerOptions { CaptureDisposableTransientServices = true });
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsTransient();

            IServiceA service1 = c.GetService<IServiceA>();
            IServiceA service2;
            using (var scope = c.CreateScope())
                service2 = scope.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.False);
            Assert.That(service2.IsDisposed, Is.True);
        }

        [Test]
        public void DisposeScope_IgnoreTransient_NotDisposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsTransient();

            IServiceA service1 = c.GetService<IServiceA>();
            IServiceA service2;
            using (var scope = c.CreateScope())
                service2 = scope.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.False);
            Assert.That(service2.IsDisposed, Is.False);
        }

        [Test]
        public void DisposeScope_CaptureScoped_Disposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsScoped();

            IServiceA service1 = c.GetService<IServiceA>();
            IServiceA service2;
            using (var scope = c.CreateScope())
                service2 = scope.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.False);
            Assert.That(service2.IsDisposed, Is.True);
        }

        [Test]
        public void DisposeScope_CaptureSingleton_NotDisposed()
        {
            ServiceContainer c = new ServiceContainer();
            c.CreateService<ServiceA>().BindTo<IServiceA>().AsSingleton();

            IServiceA service1 = c.GetService<IServiceA>();
            IServiceA service2;
            using (var scope = c.CreateScope())
                service2 = scope.GetService<IServiceA>();

            ServiceTestUtils.LogServices(service1, service2);
            Assert.That(service1.IsDisposed, Is.False);
            Assert.That(service2.IsDisposed, Is.False);
        }

        #endregion
    }
}
