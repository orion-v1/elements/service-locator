﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Orion.Elements.Ioc.ServiceLocator
{
    public static class ServiceTestUtils
    {
        private static int _lastObjectId = -1;
        private static Dictionary<Type, int> _counters = new Dictionary<Type, int>();

        public static int RequestServiceId(Type serviceType)
        {
            int serviceId = 0;

            lock (_counters)
            {
                _counters.TryGetValue(serviceType, out serviceId);
                _counters[serviceType] = serviceId + 1;
            }

            return serviceId;
        }

        public static int RequestObjectId()
        {
            return Interlocked.Increment(ref _lastObjectId);
        }

        public static void Reset()
        {
            _lastObjectId = 0;
            _counters.Clear();
        }

        public static void LogServices(IEnumerable services)
        {
            if (services != null)
                foreach (var service in services)
                    TestContext.Out.WriteLine(service.ToString());
        }

        public static void LogServices(params object[] services)
        {
            if (services != null)
                foreach (var service in services)
                    TestContext.Out.WriteLine(service.ToString());
        }

        public static string GetServiceDescription(Type serviceType, int serviceId, int objectId)
            => $"{serviceType.Name} #{serviceId} ({objectId});";

        public static string GetServiceDescription(Type serviceType, int serviceId)
            => $"{serviceType.Name} #{serviceId};";
    }
}
