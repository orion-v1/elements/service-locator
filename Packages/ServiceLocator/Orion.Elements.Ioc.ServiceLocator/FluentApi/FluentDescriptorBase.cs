﻿using System;
using System.Collections.Generic;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal abstract class FluentDescriptorBase : IServiceDescriptor
    {
        private readonly IServiceContainer _container;
        protected readonly List<ServiceBinding> _abstractions;

        public ServiceLifetime Lifetime { get; protected set; }
        public ServiceBinding[] Bindings { get => _abstractions.ToArray(); }
        public Type ImplementationType { get; protected set; }
        public object ImplementationInstance { get; protected set; }
        public Func<IServiceProvider, object> ImplementationFactory { get; protected set; }

        public FluentDescriptorBase(IServiceContainer container)
        {
            _container = container;
            _abstractions = new List<ServiceBinding>();
            Lifetime = ServiceLifetime.Singleton;
        }

        protected abstract void Build();

        public void Apply()
        {
            Build();
            _container.AddService(this);
        }
    }
}
