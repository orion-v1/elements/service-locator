﻿using System;
using System.Collections.Generic;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ServiceProviderEngineScope : IServiceScope
    {
        private bool _disposed;
        private List<IDisposable> _disposables;

        internal Dictionary<object, object> ResolvedServices { get; } // sync object
        internal ServiceProviderEngine Engine { get; }
        public IServiceContainer ServiceContainer => Engine;

        internal ServiceProviderEngineScope(ServiceProviderEngine engine)
        {
            Engine = engine;
            ResolvedServices = new Dictionary<object, object>();
        }

        public void Dispose()
        {
            lock (ResolvedServices)
            {
                if (_disposed)
                    return;
                _disposed = true;

                if (_disposables != null)
                {
                    for (int i = _disposables.Count - 1; i >= 0; i--)
                        _disposables[i].Dispose();

                    _disposables.Clear();
                }

                ResolvedServices.Clear();
            }
        }

        internal object CaptureDisposable(object service)
        {
            if (!ReferenceEquals(this, service) && service is IDisposable disposable)
                lock (ResolvedServices)
                {
                    if (_disposables == null)
                        _disposables = new List<IDisposable>();

                    _disposables.Add(disposable);
                }

            return service;
        }

        public object GetService(Type serviceType)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(IServiceProvider));

            return Engine.GetService(serviceType, this);
        }
    }
}
