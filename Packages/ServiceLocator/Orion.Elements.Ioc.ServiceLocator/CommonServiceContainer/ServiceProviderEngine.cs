﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ServiceProviderEngine : IServiceContainer
    {
        private CallSiteFactory _callSiteFactory = new CallSiteFactory();
        private CallSiteResolver _callSiteResolver = new CallSiteResolver();
        private Dictionary<Type, ServiceEntity> _implementations = new Dictionary<Type, ServiceEntity>();
        private Dictionary<Type, ServiceEntityCollection> _bindings = new Dictionary<Type, ServiceEntityCollection>();
        private bool _disposed;

        internal ServiceContainerOptions Options { get; }
        internal ServiceProviderEngineScope RootScope { get; }

        public ServiceProviderEngine(ServiceContainerOptions options)
        {
            Options = options;
            RootScope = new ServiceProviderEngineScope(this);

            AddServiceEntity<IServiceProvider>(new ConstantCallSite(this));
            AddServiceEntity<IServiceContainer>(new ConstantCallSite(this));
            AddServiceEntity<IServiceScopeFactory>(new ConstantCallSite(this));
        }

        public void Dispose()
        {
            _disposed = true;
            RootScope.Dispose();
        }

        #region IServiceContainer implementation

        public int Count => _implementations.Count;

        public object GetService(Type serviceType) => GetService(serviceType, RootScope);

        internal object GetService(Type serviceType, ServiceProviderEngineScope scope)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(IServiceContainer));

            return _callSiteResolver.Resolve(serviceType, scope);
        }

        public IServiceScope CreateScope()
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(IServiceContainer));

            return new ServiceProviderEngineScope(this);
        }

        public void AddService(IServiceDescriptor descriptor)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(IServiceContainer));

            ServiceEntity entity = new ServiceEntity(descriptor);
            entity.Validate();

            lock (_implementations)
            {
                Type implementationType = descriptor.ImplementationType;
                if (_implementations.ContainsKey(implementationType))
                    throw new InvalidOperationException(ServiceProviderUtils.CannotAddServiceExceptionMessage(
                        TypeNameHelper.GetTypeDisplayName(implementationType)));

                AddServiceEntity(entity);
            }
        }

        public bool RemoveService(IServiceDescriptor descriptor)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(IServiceContainer));

            lock (_implementations)
            {
                Type implementationType = descriptor.ImplementationType;
                if (!_implementations.TryGetValue(implementationType, out ServiceEntity entity))
                    return false;

                if (!entity.EqualsTo(descriptor))
                    return false;

                // remove bindings
                for (int i = 0; i < entity.Bindings.Length; i++)
                {
                    ServiceBinding binding = entity.Bindings[i];

                    ServiceEntityCollection sec;
                    if (_bindings.TryGetValue(binding.Type, out sec))
                    {
                        sec = sec.Remove(entity);
                        if (sec.Empty)
                            _bindings.Remove(binding.Type);
                        else
                            _bindings[binding.Type] = sec;
                    }
                }

                // remove service implementation
                _implementations.Remove(implementationType);

                return true;
            }
        }

        #endregion

        #region Entities internal management

        internal ServiceEntityCollection GetServiceEntityCollection(Type abstractionType)
        {
            lock (_implementations)
            {
                // try to get ServiceEntityCollection from collection of bindings
                ServiceEntityCollection sec;
                if (_bindings.TryGetValue(abstractionType, out sec))
                    return sec;

                // create a new ServiceEntityCollection
                sec = new ServiceEntityCollection();
                var enumerator = _implementations
                    .Where(p => p.Value.ContainsServiceAbstraction(abstractionType))
                    .Select(p => p.Value);

                foreach (var entity in enumerator)
                    sec = sec.CheckAndAdd(entity, abstractionType);

                // add the ServiceEntityCollection to collection of bindings
                _bindings[abstractionType] = sec;
                return sec;
            }
        }

        internal ICallSite GetServiceCallSite(ServiceEntity entity, ServiceProviderEngineScope scope, CallSiteChain chain)
        {
            // OPTIMIZATION: Potentially, this is a very frequent operation.
            // Using locking inside can cause performance issues.
            // For optimization, use a more efficient approach than lock().

            ICallSite callSite = entity.CallSite;
            if (callSite == null)
                lock (entity)
                {
                    if (entity.CallSite == null)
                        entity.CallSite = _callSiteFactory.CreateCallSite(entity, scope, chain);

                    callSite = entity.CallSite;
                }

            return callSite;
        }

        private void AddServiceEntity<TService>(ICallSite callSite)
        {
            ServiceBinding binding = new ServiceBinding
            {
                Type = typeof(TService),
                Options = ServiceBindingOptions.Default
            };

            ServiceEntity entity = new ServiceEntity(
                ServiceLifetime.Singleton,
                new[] { binding },
                typeof(TService),
                null,
                null);

            entity.CallSite = callSite;

            AddServiceEntity(entity);
        }

        private void AddServiceEntity(ServiceEntity entity)
        {
            int count = entity.Bindings.Length;
            ServiceEntityCollection[] entityCollections = new ServiceEntityCollection[count];

            // Checks constraints before adding service to the container.
            // It's highly important to do it before changing something in the container.
            for (int i = 0; i < count; i++)
            {
                ServiceBinding binding = entity.Bindings[i];

                // adding a new entity to sec can throw an exception 
                _bindings.TryGetValue(binding.Type, out var sec);
                entityCollections[i] = sec.CheckAndAdd(entity, binding);
            }

            // add all abstractions of the service to collection
            for (int i = 0; i < count; i++)
            {
                ServiceBinding binding = entity.Bindings[i];
                _bindings[binding.Type] = entityCollections[i];
            }

            // add service implementation
            _implementations.Add(entity.ImplementationType, entity);
        }

        #endregion
    }
}
