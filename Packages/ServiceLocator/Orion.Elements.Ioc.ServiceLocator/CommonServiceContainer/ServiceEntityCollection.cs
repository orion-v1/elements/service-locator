﻿using System;
using System.Collections.Generic;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal struct ServiceEntityCollection
    {
        private ServiceEntity _entity;
        private List<ServiceEntity> _entities;
        private ServiceBindingOptions _bindingOptions;

        public bool Empty => _entity == null && _entities == null;

        public int Count
        {
            get
            {
                if (_entities != null)
                    return _entities.Count;

                return _entity != null ? 1 : 0;
            }
        }

        public ServiceEntity Last => _entities != null 
            ? _entities[_entities.Count - 1]
            : _entity;

        public ServiceEntity this[int index]
        {
            get
            {
                if (_entities != null)
                    return _entities[index];

                if (index == 0)
                    return _entity;

                throw new IndexOutOfRangeException();
            }
        }

        public ServiceEntityCollection CheckAndAdd(ServiceEntity entity, ServiceBinding binding)
        {
            int count = entity.Bindings.Length;

            // is it an attempt to override an exclusive binding?
            if ((_bindingOptions & ServiceBindingOptions.Exclusive) != 0)
                throw new InvalidOperationException(ServiceProviderUtils.CannotOverrideExclusiveBindingExceptionMessage(
                    TypeNameHelper.GetTypeDisplayName(entity.ImplementationType),
                    TypeNameHelper.GetTypeDisplayName(binding.Type)));

            // is it an attempt to add an exclusive binding to not empty collection?
            if ((binding.Options & ServiceBindingOptions.Exclusive) != 0 && !Empty)
                throw new InvalidOperationException(ServiceProviderUtils.CannotApplyExclusiveBindingExceptionMessage(
                    TypeNameHelper.GetTypeDisplayName(entity.ImplementationType),
                    TypeNameHelper.GetTypeDisplayName(binding.Type)));

            return Add(entity, binding.Options);
        }

        public ServiceEntityCollection CheckAndAdd(ServiceEntity entity, Type abstractionType)
            => CheckAndAdd(entity, entity.GetServiceAbstraction(abstractionType));

        private ServiceEntityCollection Add(ServiceEntity entity, ServiceBindingOptions bindingOptions)
        {
            ServiceEntityCollection newItem = new ServiceEntityCollection();
            newItem._bindingOptions = bindingOptions;

            if (_entities != null)
            {
                newItem._entity = null;
                newItem._entities = _entities;
                newItem._entities.Add(entity);
                return newItem;
            }

            if (_entity == null)
            {
                newItem._entity = entity;
                return newItem;
            }

            newItem._entity = null;
            newItem._entities = new List<ServiceEntity> { _entity, entity };
            return newItem;
        }

        public ServiceEntityCollection Remove(ServiceEntity entity)
        {
            ServiceEntityCollection newItem = new ServiceEntityCollection();
            newItem._bindingOptions = _bindingOptions;

            if (_entity != null)
            {
                newItem._entity = _entity == entity ? null : _entity;
                newItem._entities = null;
            }

            else if (_entities != null)
            {
                _entities.Remove(entity);

                if (_entities.Count > 1)
                {
                    newItem._entity = null;
                    newItem._entities = _entities;
                }
                else
                {
                    newItem._entity = _entities[0];
                    newItem._entities = null;
                    _entities.Clear();
                }
            }

            return newItem;
        }
    }
}
