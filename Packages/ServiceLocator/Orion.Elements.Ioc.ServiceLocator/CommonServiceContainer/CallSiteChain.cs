﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class CallSiteChain
    {
        private int _order = 0;
        private Dictionary<Type, int> _services = new Dictionary<Type, int>();

        public void Add(Type implementationType)
        {
            if (_services.ContainsKey(implementationType))
                throw new InvalidOperationException(CreateCircularDependencyExceptionMessage(implementationType));

            _services.Add(implementationType, _order);
            _order++;
        }

        public bool Remove(Type implementationType)
        {
            return _services.Remove(implementationType);
        }

        private string CreateCircularDependencyExceptionMessage(Type type)
        {
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendFormat(ServiceProviderUtils.CircularDependencyExceptionMessageFormat, TypeNameHelper.GetTypeDisplayName(type));
            messageBuilder.AppendLine();

            AppendResolutionPath(messageBuilder, type);

            return messageBuilder.ToString();
        }

        private void AppendResolutionPath(StringBuilder builder, Type currentlyResolving = null)
        {
            foreach (var pair in _services.OrderBy(p => p.Value))
            {
                builder.AppendFormat("{0} ->", TypeNameHelper.GetTypeDisplayName(pair.Key));
                builder.AppendLine();
            }

            builder.Append(TypeNameHelper.GetTypeDisplayName(currentlyResolving));
        }
    }
}
