﻿using System;
using System.Collections.Generic;

namespace Orion.Elements.Ioc.ServiceLocator
{
    /// <summary>
    /// The default IServiceProvider.
    /// </summary>
    public sealed class ServiceContainer : IServiceContainer, IDisposable
    {
        private readonly ServiceProviderEngine _engine;

        /// <summary>
        /// Creates a new container with specified <paramref name="options"/>
        /// </summary>
        /// <param name="options">Options to configure the container. Can be null.</param>
        public ServiceContainer(ServiceContainerOptions options = null)
        {
            if (options == null)
                options = ServiceContainerOptions.Default;

            switch (options.Mode)
            {
                case ServiceContainerMode.RuntimeMultiThread:
                    _engine = new ServiceProviderEngine(options);
                    break;

                default:
                    throw new NotSupportedException(nameof(options.Mode));
            }
        }

        /// <inheritdoc />
        public void Dispose() => _engine.Dispose();

        /// <inheritdoc />
        public int Count => _engine.Count;

        /// <inheritdoc />
        public IServiceScope CreateScope() => _engine.CreateScope();

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType) => _engine.GetService(serviceType);

        /// <inheritdoc />
        public void AddService(IServiceDescriptor descriptor) => _engine.AddService(descriptor);

        /// <inheritdoc />
        public bool RemoveService(IServiceDescriptor descriptor) => _engine.RemoveService(descriptor);
    }
}
