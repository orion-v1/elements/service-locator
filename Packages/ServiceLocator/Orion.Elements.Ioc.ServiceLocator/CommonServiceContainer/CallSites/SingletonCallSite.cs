﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class SingletonCallSite : ScopedCallSite
    {
        public SingletonCallSite(ICallSite callSite, object cacheKey) : base(callSite, cacheKey)
        {
        }

        public override CallSiteKind Kind => CallSiteKind.Singleton;

        public override object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitSingletonCallSite(this, scope);
    }
}
