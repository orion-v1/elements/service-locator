﻿using System;
using System.Reflection;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ConstructorCallSite : ICallSite
    {
        public readonly ConstructorInfo ConstructorInfo;
        public readonly Type[] ServiceTypes;

        public ConstructorCallSite(ConstructorInfo constructorInfo, Type[] serviceTypes)
        {
            @ConstructorInfo = constructorInfo;
            this.ServiceTypes = serviceTypes;
        }

        public CallSiteKind Kind => CallSiteKind.Constructor;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitConstructorCallSite(this, scope);
    }
}
