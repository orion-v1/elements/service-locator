﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ActivatorCallSite : ICallSite
    {
        public readonly Type ImplementationType;

        public ActivatorCallSite(Type implementationType)
        {
            ImplementationType = implementationType;
        }

        public CallSiteKind Kind => CallSiteKind.Activator;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitActivatorCallSite(this, scope);
    }
}
