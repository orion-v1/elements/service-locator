﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ScopedCallSite : ICallSite
    {
        internal ICallSite CallSite { get; }
        public object CacheKey { get; }

        public ScopedCallSite(ICallSite callSite, object cacheKey)
        {
            CallSite = callSite;
            CacheKey = cacheKey;
        }

        public virtual CallSiteKind Kind => CallSiteKind.Scope;

        public virtual object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitScopedCallSite(this, scope);
    }
}
