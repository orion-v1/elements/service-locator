﻿using System;

namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class ConstantCallSite : ICallSite
    {
        public readonly object ImplementationInstance;

        public ConstantCallSite(object implementationInstance)
        {
            ImplementationInstance = implementationInstance;
        }

        public CallSiteKind Kind => CallSiteKind.Constant;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitConstantCallSite(this, scope);
    }
}
