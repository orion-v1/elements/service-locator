﻿namespace Orion.Elements.Ioc.ServiceLocator
{
    internal class TransientCallSite : ICallSite
    {
        internal ICallSite CallSite { get; }

        public TransientCallSite(ICallSite callSite)
        {
            CallSite = callSite;
        }

        public CallSiteKind Kind => CallSiteKind.Transient;

        public object Accept(ICallSiteVisitor visitor, ServiceProviderEngineScope scope) => visitor.VisitTransientCallSite(this, scope);
    }
}
